﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Company;
using NFCProject.Core.Constants;
using NFCProject.Core.Entities;
using NFCProject.Data.DbContexts;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.Service
{
    public class CompanyService : BaseService<Company>, ICompanyService
    {
        public CompanyService(NFCDbContext db) : base(db)
        {

        }

        public async Task<List<PaymentDetail>> GetAllPaymentDetailByCompanyId(ByIdRequestModel model)
        {
            var list = new List<PaymentDetail>();
            if (model != null)
            {
                var findCompany = await _db.Company.Where(x => x.UserId == model.Id).FirstOrDefaultAsync();

                var getAllCardByCompanyId = await _db.Card.Where(x => x.CompanyId == findCompany.Id && !x.IsDeleted).ToListAsync();

                foreach (var item in getAllCardByCompanyId)
                {
                    var paymentDetail = await _db.PaymentDetail.Include(b => b.Card).Include(x => x.MemberBusiness).Where(b => !b.IsDeleted && b.CardId == item.Id).ToListAsync();

                    list.AddRange(paymentDetail);
                }
            }

            return list;

        }

        public async Task<List<PaymentDetail>> GetAllPaymentDetailByCardId(ByIdRequestModel model)
        {
            if (model != null)
            {
                var paymentDetail = await _db.PaymentDetail.Include(b => b.Card).Include(x => x.MemberBusiness).Where(b => !b.IsDeleted && b.CardId == model.Id).ToListAsync();
                return paymentDetail;
            }

            return new List<PaymentDetail>();

        }
        public async Task<BaseRequestModel> GetAllCompanies()
        {
            var returnModel = new BaseRequestModel();
            var company = await _db.Company.Where(t => !t.IsDeleted).Include(t=>t.User).ToListAsync();

            returnModel.Data = company;
            returnModel.Detail = "Firmalar Listelendi";
            return returnModel;
        }

        public async Task<BaseRequestModel> AddCompany(AddCompanyRequestModel model)
        {
            var returnModel = new BaseRequestModel();

            var ifExistMail = await _db.Company.Where(t => !t.IsDeleted && (t.Email == model.Email)).FirstOrDefaultAsync();
            if (ifExistMail != null)
            {
                returnModel.IsError = true;
                returnModel.Detail = "Girilen E-Posta sistemde kayıtlı, farklı bir E-posta giriniz!";
                return returnModel;

            }

            Random random = new Random();

            var role = await _db.Role.Where(x => !x.IsDeleted && x.Name == Constants.Role.Company.ToString()).FirstOrDefaultAsync();

            User user = new User()
            {
                UserName = random.Next(100000, 999999).ToString(),
                FullName = model.Name,
                Password = "12345",
                RoleId = role.Id,
                RecordDate = DateTime.Now,
            };
            _db.User.Add(user);
            var save = _db.SaveChanges();
            if (save > 0)
            {
                Company company = model.Adapt<Company>();
                company.UserId = user.Id;
                _db.Company.Add(company);
                _db.SaveChanges();
                returnModel.Detail = "Firma başarıyla eklendi";

            }
            else
            {
                returnModel.IsError = true;
                returnModel.Detail = "Firma eklerken hata oluştu, tekrar deneyiniz!";
            }

            return returnModel;
        }

        public async Task<BaseRequestModel> GetById(int? Id)
        {
            var returnModel = new BaseRequestModel();
            var getCompany = await _db.Company.Where(t => !t.IsDeleted && t.Id == Id).FirstOrDefaultAsync();
            if (getCompany == null)
            {
                returnModel.Detail = "Firma bulunamadı!";
                returnModel.IsError = true;
            }
            else
            {
                returnModel.Data = getCompany.Adapt<GetByIdCompanyRequestModel>();
                returnModel.Detail = "Firma detayı getirildi.";
            }
            return returnModel;
        }

        public async Task<BaseRequestModel> UpdateCompany(GetByIdCompanyRequestModel model)
        {
            var returnModel = new BaseRequestModel();

           

            var getCompany = await _db.Company.Where(t => !t.IsDeleted && t.Id == model.Id).FirstOrDefaultAsync();
            if (getCompany == null)
            {
                returnModel.IsError = true;
                returnModel.Detail = "Firma bulunamadı";
            }
            else
            {
                getCompany.Name = model.Name;
                getCompany.TaxNo = model.TaxNo;
                getCompany.PhoneNumber = model.PhoneNumber;
                getCompany.Email = model.Email;
                getCompany.Address = model.Address;
                getCompany.AuthorizedPerson = model.AuthorizedPerson;


                var save = _db.SaveChanges();
                returnModel.Detail = "Firma başarıyla güncellendi";
            }
            return returnModel;
        }

        public async Task<BaseRequestModel> DeleteCompany(int? Id)
        {
            var returnModel = new BaseRequestModel();
            var getCompany = await _db.Company.Where(t => !t.IsDeleted && t.Id == Id).FirstOrDefaultAsync();
            if (getCompany == null)
            {
                returnModel.IsError = true;
                returnModel.Detail = "Firma bulunamadı";
            }
            else
            {
                getCompany.IsDeleted = true;
                _db.SaveChanges();
            }
            return returnModel;
        }
    }
}
