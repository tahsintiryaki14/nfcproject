﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.ResponseModel
{
    public class BaseResultModel
    {
        public string Detail { get; set; }
        public bool IsError { get; set; }
        public int IsSuccess { get; set; }
    }
}
