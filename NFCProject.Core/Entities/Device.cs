﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Entities
{
    public class Device : BaseEntity
    {
        public string Name { get; set; }
        public string SerialNumber { get; set; }
        public bool IsEmpty { get; set; }
        public virtual List<MemberBusiness> MemberBusinessList { get; set; } = new List<MemberBusiness>();
    }
}
