﻿using Microsoft.AspNetCore.Mvc;
using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Card;
using NFCProject.Core.Entities;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFCProject.UI.Controllers
{
    public class CardController : Controller
    {
        private readonly IMemberService _memberService;
        private readonly ICompanyService _companyService;
        private readonly ICardService _cardService;
        public CardController(IMemberService memberService, ICompanyService companyService, ICardService cardService)
        {
            _memberService = memberService;
            _companyService = companyService;
            _cardService = cardService;
        }
        public async Task<IActionResult> Index()
        {
            var result = await _memberService.GetAllCards();
            if(result.Data!=null)
            {
                ViewBag.CardList = (List<GetCardRequestModel>)result.Data;
            }
            return View();
        }
        public async Task<IActionResult> AddCard()
        {
            var result = await _companyService.GetAllCompanies();
            if (result.Data != null)
            {
                ViewBag.Companies = (List<Company>)result.Data;
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddCard(AddCardRequestModel model)
        {
            var result = await _cardService.AddCard(model);
            if (result.IsError == true)
            {
                return Json(new { result = "Error", message = result.Detail });
            }
            else
            {
                return Json(new { result = "Success", message = result.Detail });
            }

        }
        [HttpPost]
        public async Task<IActionResult> CardAddedAmount(CardAddAmountRequestModel model)
        {
            var result = await _cardService.CardAddAmount(model);
            if (result.IsError == true)
            {
                return Json(new { result = "Error", message = result.Detail });
            }
            else
            {
                return Json(new { result = "Success", message = result.Detail });
            }

        }
        [HttpPost]
        public async Task<IActionResult> DeleteCard(ByIdRequestModel model)
        {
            var result = await _cardService.DeleteCard(model);
            if (result.IsError == true)
            {
                return Json(new { result = "Error", message = result.Detail });
            }
            else
            {
                return Json(new { result = "Success", message = result.Detail });
            }

        }
    }
}
