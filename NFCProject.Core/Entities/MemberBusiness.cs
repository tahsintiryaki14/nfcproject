﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Entities
{
    public class MemberBusiness : BaseEntity
    {
        public string Name { get; set; }
        public string TaxNo { get; set; }
        public int DeviceId { get; set; }
        public Device Device { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ServiceEndustry ServiceEndustry { get; set; }
        public int ServiceEndustryId { get; set; }
        public virtual List<PaymentDetail> PaymentDetailList { get; set; } = new List<PaymentDetail>();

    }
}
