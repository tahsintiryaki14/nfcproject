﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Card
{
    public class CardAddAmountRequestModel
    {
        public int CardId { get; set; }
        public decimal CardAddAmount{ get; set; }
    }
}
