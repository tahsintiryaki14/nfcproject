﻿using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Company;
using NFCProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.IService
{
    public  interface ICompanyService: IService<Company>
    {
        Task<List<PaymentDetail>> GetAllPaymentDetailByCompanyId(ByIdRequestModel model);
        Task<BaseRequestModel> GetAllCompanies();
        Task<List<PaymentDetail>> GetAllPaymentDetailByCardId(ByIdRequestModel model);
        Task<BaseRequestModel> AddCompany(AddCompanyRequestModel model);
        Task<BaseRequestModel> GetById(int? Id);
        Task<BaseRequestModel> DeleteCompany(int? Id);
        Task<BaseRequestModel> UpdateCompany(GetByIdCompanyRequestModel model);
    }
}
