﻿using NFCProject.Data.DbContexts;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.Service
{
    public class BaseService<T> : IService<T> where T : class
    {
        public readonly NFCDbContext _db;

        public BaseService(NFCDbContext db)
        {
            _db = db;
        }

    }

}
