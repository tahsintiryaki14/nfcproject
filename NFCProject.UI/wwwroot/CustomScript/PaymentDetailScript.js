﻿

$("#getAllPaymentByCompanyIdForm").submit(function (event) {
    event.preventDefault();

    debugger;

    var CardId = $("#CardId").val();

    if (CardId == "") {
        toastr.error("Lütfen kart seçiniz", "Uyarı");
    }
    else {

        var formValue = (this);

        $.ajax({
            method: "POST",
            url: "/Company/GetAllPaymentDetailByCardId",
            //dataType: 'json',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            data: new FormData(formValue),
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {

            }
        }).done(function (d) {
            if (d.failed == true) {
                swal({
                    title: "Hata",
                    text: d.message,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: '#5cb85c',
                    confirmButtonText: 'Tamam',
                    cancelButtonText: "Kapat",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    allowOutsideClick: false
                }).then(function (willDelete) {
                    if (willDelete) {
                    }

                });
            }
            else {

                toastr.success("Ödeme detayları getirildi.", "Başarılı");
                $("#paymentDetailList").html(d);
            }

        }).fail(function (xhr) {
            if (xhr.status == 403 || xhr.status == 401 || xhr.status == 404) {
                window.location.href = "/Account/AccessDenied";
            }
            else {
                toastr.error("Hata oluştu", "Hata");
            }
        }).always(function () {

        });
    }

});