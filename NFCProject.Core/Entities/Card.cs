﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Entities
{
    public class Card : BaseEntity
    {
        public string CardGuidId { get; set; }
        public bool IsActive { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }
        public string FullName { get; set; }
        public decimal Balance { get; set; }
        public decimal RemainingAmount { get; set; }


        public virtual List<PaymentDetail> PaymentDetailList { get; set; } = new List<PaymentDetail>();
    }
}
