﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Entities
{
    public class PaymentTransactions
    {
        public int Id { get; set; }
        public int MemberId { get; set; } 
        public decimal Amount { get; set; }
        public string TransactionKey { get; set; }
        public bool IsUsed { get; set; } 
        public string CardGuidId { get; set; }
        public DateTime RecordDate { get; set; }
        public DateTime? PaymentDate { get; set; } 

    }
}
