﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Device;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFCProject.UI.Controllers
{
    public class DeviceController : Controller
    {
        private readonly IDeviceService _deviceService;

        public DeviceController(IDeviceService deviceService)
        {
            _deviceService = deviceService;
        }
        public async Task<IActionResult> Index()
        {
            var response = await _deviceService.GetAllDevices();
            return View(response.Data);
        }
        public async Task<IActionResult> GetAllDeviceforPartial()
        {
            var response = await _deviceService.GetAllDevices();
            return PartialView("~/Views/Device/_PartialGetAllDevices.cshtml", response.Data);
        }
        [HttpPost]
        public async Task<IActionResult> AddDevice(AddDeviceRequestModel model)
        {
            var user = HttpContext.Session.GetString("UserId");
            if (!string.IsNullOrEmpty(user))
            {
                model.Id = Convert.ToInt32(user);
                var response = await _deviceService.AddDevice(model);
                if (response.IsError == true)
                {
                    return Json(new { failed = true, message = response.Detail });
                }
                else
                {
                    return Json(new { failed = false, message = response.Detail });
                }
            }
            return Json(new { failed = true, message = "Kullanıcı Bulunamadı" });
        }
        public async Task<IActionResult> DeleteDevice(ByIdRequestModel model)
        {
            var user = HttpContext.Session.GetString("UserId");
            if (!string.IsNullOrEmpty(user))
            {
                model.UserId = Convert.ToInt32(user);
                var response = await _deviceService.DeleteDevice(model);
                if (response.IsError == true)
                {
                    return Json(new { failed = true, message = response.Detail });
                }
                else
                {
                    return Json(new { failed = false, message = response.Detail });
                }
            }
            return Json(new { failed = true, message = "Kullanıcı Bulunamadı" });
        }
        public async Task<IActionResult> UpdateDeviceById(ByIdRequestModel model)
        {
            var response = await _deviceService.UpdateDeviceById(model);
            if (response.IsError == true)
            {
                return Json(new { failed = true, message = response.Detail });
            }
            else
            {
                return PartialView("~/Views/Device/_PartialUpdateDeviceByıd.cshtml", response.Data);
            }
        }
    }
}
