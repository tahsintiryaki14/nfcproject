﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Card;
using NFCProject.Contact.RequestModel.Device;
using NFCProject.Contact.RequestModel.Payment;
using NFCProject.Core.Entities;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFCProject.UI.Controllers
{
    public class MemberBusinessController : Controller
    {
        private readonly IMemberService _memberService;
        private readonly ICompanyService _companyService;
        private readonly IDeviceService _deviceService;

        public MemberBusinessController(IMemberService memberService, ICompanyService companyService, IDeviceService deviceService)
        {
            _memberService = memberService;
            _companyService = companyService;
            _deviceService = deviceService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> CardShooting()
        {

            var response = await _memberService.GetAllCards();
            ViewBag.CardList = response.Data;

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CardPayment(PaymentCardRequestModel model)
        {
            var user = HttpContext.Session.GetString("UserId");
            if (!string.IsNullOrEmpty(user))
            {
                model.Id = Convert.ToInt32(user);

                var response = await _memberService.CardPayment(model);

                if (response.IsError == true)
                {
                    return Json(new { failed = true, message = response.Detail });
                }
                else
                {
                    return Json(new { failed = false, message = response.Detail });
                }
            }

            return Json(new { failed = true, message = "Ödeme Bilgileri Alınamadı." });
        }
        public async Task<IActionResult> PaymentTransactions()
        {
            var model = new ByIdRequestModel();
            var user = HttpContext.Session.GetString("UserId");
            if (!string.IsNullOrEmpty(user))
            {
                model.Id = Convert.ToInt32(user);
                var response = await _memberService.GetPaymentDetailsforMemberBussiness(model);
                return View(response.Data);
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> GetFilteredPaymentTransactions(GetFilteredPaymentTransactionsRequestModel model)
        {
            var user = HttpContext.Session.GetString("UserId");
            if (!string.IsNullOrEmpty(user))
            {
                model.Id = Convert.ToInt32(user);
                var response = await _memberService.GetFilteredPaymentTransactions(model);

                if (response.IsError == true)
                {
                    return Json(new { failed = true, message = response.Detail });
                }
                else
                {
                    return PartialView("~/Views/MemberBusiness/_PartialGetPaymentTransactions.cshtml", response.Data);
                }
            }
            else
            {
                return Json(new { failed = true, message = "Kullanıcı Bilgisi Bulunamadı" });
            }
        }
        public async Task<IActionResult> AddMemberBusiness()
        {
            ViewBag.ServiceList = await _memberService.GetAllServiceIndustry();
            var data = await _deviceService.GetAllEmptyDevices();
            ViewBag.DeviceList = (List<GetDeviceRequestModel>)data.Data;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddMemberBusiness(MemberBusiness model)
        {
            var result = await _memberService.AddMemberBusiness(model);
            if (result.IsError == true)
            {
                return Json(new { result = "Error", message = result.Detail });
            }
            else
            {
                return Json(new { result = "Success", message = result.Detail });
            }

        }

        public async Task<IActionResult> MemberBusinessList()
        {
            var list = await _memberService.GetAllMemberBusiness();
            return View(list);
        }

    }
}
