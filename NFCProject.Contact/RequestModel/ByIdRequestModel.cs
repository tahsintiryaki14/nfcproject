﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel
{
    public class ByIdRequestModel
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
    }
}
