﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel
{
    public class BaseRequestModel
    {
        public string Detail { get; set; }
        public bool IsError { get; set; }
        public object Data { get; set; }
        public int IsSuccess { get; set; } 
    }
}
