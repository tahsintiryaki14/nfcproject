﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NFCProject.Contact.RequestModel;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFCProject.UI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AccountController(IHttpContextAccessor httpContextAccessor,IUserService userService)
        {
            _httpContextAccessor = httpContextAccessor;
            _userService = userService;
        }
        public IActionResult Login(string hata)
        {
            if (hata != null)
            {
                ViewBag.ErrorMessage = "Kullanıcı adı veya parola hatalı!";
            }
            return View();
        }


        [HttpPost]
        public IActionResult Login(UserRequestModel model)
        {
            if (model.UserName == null || model.Password == null)
            {
                return View();
            }

            var user = _userService.GetUser(model);
            if (user != null)
            {
                var keyUserId = "UserId";
                var strUserId = JsonConvert.SerializeObject(user.Id);
                _httpContextAccessor.HttpContext.Session.SetString(keyUserId, strUserId);

                var userNameKeyId = "UserName";
                var strUserName = JsonConvert.SerializeObject(user.FullName);
                _httpContextAccessor.HttpContext.Session.SetString(userNameKeyId, strUserName);

                var keyuserRole = "UserRole";
                var strUserRole = JsonConvert.SerializeObject(user.Role.Name);
                _httpContextAccessor.HttpContext.Session.SetString(keyuserRole, strUserRole);


                return RedirectToAction("Index", "Home");

            }

            return RedirectToAction("Login", "Account", new { hata = "Kullanıcı adı veya şifre hatalı!" });

        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "Account");
        }


    }
}
