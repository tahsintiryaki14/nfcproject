﻿using NFCProject.Contact.RequestModel.Card;
using NFCProject.Contact.RequestModel.MemberBussiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Payment
{
    public class GetPaymentDetailsRequestModel
    {
        public int Id { get; set; }
        public int CardId { get; set; }
        public GetCardRequestModel Card { get; set; } = new GetCardRequestModel();
        public int MemberBusinessId { get; set; }
        public GetMemberBussinessRequestModel MemberBusiness { get; set; } = new GetMemberBussinessRequestModel();
        public DateTime ExecutionDate { get; set; }
        public decimal Amount { get; set; }
        public DateTime RecordDate { get; set; }
    }
}
