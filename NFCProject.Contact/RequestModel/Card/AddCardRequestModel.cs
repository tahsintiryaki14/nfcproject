﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Card
{
    public class AddCardRequestModel
    {
        public int Id { get; set; }
        public string CardGuidId { get; set; }
        public bool IsActive { get; set; }
        public int CompanyId { get; set; }
        public decimal Balance { get; set; }
       
    }
}
