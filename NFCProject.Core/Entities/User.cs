﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Entities
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public virtual List<MemberBusiness> MemberBusinessList { get; set; } = new List<MemberBusiness>();
        public virtual List<Company> CompanyList { get; set; } = new List<Company>();
    }
}
