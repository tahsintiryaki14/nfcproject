﻿using NFCProject.Contact.RequestModel;
using NFCProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.IService
{
    public interface IUserService : IService<User>
    {
        User GetUser(UserRequestModel model);
    }
}
