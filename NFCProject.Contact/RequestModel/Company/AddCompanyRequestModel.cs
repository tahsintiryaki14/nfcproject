﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Company
{
   public  class AddCompanyRequestModel
    {
        public string Name { get; set; }
        public string TaxNo { get; set; }
        public string Address { get; set; }
        public string AuthorizedPerson { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
