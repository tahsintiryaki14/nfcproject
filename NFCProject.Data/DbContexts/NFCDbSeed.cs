﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NFCProject.Core.Constants;
using NFCProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;


namespace NFCProject.Data.DbContexts
{
    public static  class NFCDbSeed
    {
        private static void GetAndAddEnums<TEntity, TEnum>(this DbContext dbContext, bool create,
         IDictionary<TEnum, int> ids) where TEntity : BaseEnum
        {
            foreach (TEnum e in Enum.GetValues(typeof(TEnum)))
            {
                var name = e.ToString();
                var query = dbContext.Set<TEntity>().SingleOrDefault(q => q.Name == name);
                if (query != null)
                {
                    ids.Add(e, query.Id);
                }
                else if (create)
                {
                    var memberInfo = typeof(TEnum).GetMember(name)[0];
                    var hasDisplayAttribute = Attribute.IsDefined(memberInfo, typeof(DisplayAttribute));
                    var displayAttribute =
                        hasDisplayAttribute ? memberInfo.GetCustomAttribute<DisplayAttribute>() : null;

                    if (displayAttribute?.Description != null)
                    {
                        var newEntity =
                            (TEntity)Activator.CreateInstance(typeof(TEntity), name, displayAttribute.Description);
                        newEntity.RecordDate = DateTime.Now;
                        var entity = dbContext.Set<TEntity>().Add(newEntity).Entity;
                        dbContext.SaveChanges();
                        ids.Add(e, entity.Id);
                    }
                    else
                    {
                        var newEntity = (TEntity)Activator.CreateInstance(typeof(TEntity), name);
                        var entity = dbContext.Set<TEntity>().Add(newEntity).Entity;
                        dbContext.SaveChanges();
                        ids.Add(e, entity.Id);
                    }
                }
            }
        }
        public static async void Initialize(IServiceProvider serviceProvider, bool create = false)
        {
            using var serviceScope = serviceProvider.CreateScope();
            var dbContext = serviceScope.ServiceProvider.GetService<NFCDbContext>();

            dbContext.Database.Migrate();

            dbContext.GetAndAddEnums<ServiceEndustry, Constants.ServiceEndustry>(create, Constants.ServiceEndustryIds);
            dbContext.GetAndAddEnums<Role, Constants.Role>(create, Constants.RoleIds);

            //var cardList = new List<string> { "123456", "1234567","" };


            var user = dbContext.User.Where(x => x.UserName == "admin").FirstOrDefault();
            if (user == null)
            {
                user = new User();
                user.FullName = "Admin";
                user.UserName = "admin";
                user.Password = "12345";
                var userRole = dbContext.Role.Where(x => x.Name == Constants.Role.Admin.ToString()).FirstOrDefault();

                if (userRole != null)
                {
                    user.RoleId = userRole.Id;
                }

                //user.Password = userTcList[i].Substring(5);

                dbContext.User.Add(user);


                var memberUser = new User();
                memberUser.FullName = "Balcan";
                memberUser.UserName = "balcan";
                memberUser.Password = "12345";
                var memberBusinessRole = dbContext.Role.Where(x => x.Name == Constants.Role.MemberBusiness.ToString()).FirstOrDefault();

                if (memberBusinessRole != null)
                {
                    memberUser.RoleId = memberBusinessRole.Id;
                }

                //user.Password = userTcList[i].Substring(5);

                dbContext.User.Add(memberUser);

                var companyUser = new User();
                companyUser.FullName = "Farmakod";
                companyUser.UserName = "farmakod";
                companyUser.Password = "12345";
                var companyRole = dbContext.Role.Where(x => x.Name == Constants.Role.Company.ToString()).FirstOrDefault();

                if (companyRole != null)
                {
                    companyUser.RoleId = companyRole.Id;
                }

                //user.Password = userTcList[i].Substring(5);

                dbContext.User.Add(companyUser);

                dbContext.SaveChanges();

                var company = dbContext.Company.Where(x => x.Name == "Farmakod").FirstOrDefault();

                if (company == null)
                {
                    company = new Company();
                    company.Name = "Farmakod";
                    company.UserId = companyUser.Id;

                    dbContext.Company.Add(company);
                }


                var memberBusiness = dbContext.MemberBusiness.Where(x => x.Name == "Balcan").FirstOrDefault();
                var serviceType = dbContext.ServiceEndustry.Where(x => x.Name == Constants.ServiceEndustry.Food.ToString()).FirstOrDefault();

                var device = new Device();
                device.Name = "Setcard pos cihazı";
                device.SerialNumber = "123";
                dbContext.Device.Add(device);
                dbContext.SaveChanges();

                if (memberBusiness == null)
                {
                    memberBusiness = new MemberBusiness();
                    memberBusiness.Name = "Balcan";
                    memberBusiness.UserId = memberUser.Id;
                    memberBusiness.DeviceId = device.Id;
                    memberBusiness.ServiceEndustryId = serviceType.Id;

                    dbContext.MemberBusiness.Add(memberBusiness);
              

                }

                dbContext.SaveChanges();
            }

        }
    }
}
