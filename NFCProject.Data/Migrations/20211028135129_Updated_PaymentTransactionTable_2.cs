﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NFCProject.Data.Migrations
{
    public partial class Updated_PaymentTransactionTable_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsUsed",
                table: "PaymentTransactions",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUsed",
                table: "PaymentTransactions");
        }
    }
}
