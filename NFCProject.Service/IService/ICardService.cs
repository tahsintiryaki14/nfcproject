﻿using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Card;
using NFCProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.IService
{
    public interface ICardService : IService<Card>
    {
        Task<BaseRequestModel> AddCard(AddCardRequestModel model);
        Task<BaseRequestModel> CardAddAmount(CardAddAmountRequestModel model);
        Task<BaseRequestModel> DeleteCard(ByIdRequestModel model);
        Task<List<GetCardRequestModel>> UpdateCardName(int Id, string FullName);
    }
}
