﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Entities
{
    public class PaymentDetail : BaseEntity
    {
        public int CardId { get; set; }
        public Card Card { get; set; }
        public int MemberBusinessId { get; set; }
        public MemberBusiness MemberBusiness { get; set; }
        public DateTime ExecutionDate { get; set; }
        public decimal Amount { get; set; }
    }
}
