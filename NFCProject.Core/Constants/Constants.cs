﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Constants
{
    public static partial class Constants
    {
        public static Dictionary<Role, int> RoleIds { get; set; } = new Dictionary<Role, int>();
        public enum Role
        {
            [Display(Description = "Admin")] Admin,
            [Display(Description = "Üye iş yeri")] MemberBusiness,
            [Display(Description = "Firma")] Company,

        }


        public static Dictionary<ServiceEndustry, int> ServiceEndustryIds { get; set; } = new Dictionary<ServiceEndustry, int>();
        public enum ServiceEndustry
        {
            [Display(Description = "Gıda")] Food,
            [Display(Description = "Servis")] Service,
        }


    }
}
