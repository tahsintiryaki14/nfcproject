﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Card;
using NFCProject.Core.Entities;
using NFCProject.Data.DbContexts;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.Service
{
    public class CardService : BaseService<Card>, ICardService
    {
        public CardService(NFCDbContext db) : base(db)
        {

        }

        public async Task<BaseRequestModel> AddCard(AddCardRequestModel model)
        {
            var returnModel = new BaseRequestModel();
            var checkCompany = await _db.Company.Where(t => !t.IsDeleted && t.Id == model.CompanyId).FirstOrDefaultAsync();
            if (checkCompany == null)
            {
                returnModel.IsError = true;
                returnModel.Detail = "Firma bulunamadı";
                return returnModel;
            }
            var checkCard = await _db.Card.Where(t => !t.IsDeleted && t.CardGuidId == model.CardGuidId).FirstOrDefaultAsync();
            if (checkCard != null)
            {
                returnModel.IsError = true;
                returnModel.Detail = "Kart sistemde kayıtlı, tekrar deneyiniz";
                return returnModel;
            }
            var card = model.Adapt<Card>();
            card.IsActive = true;
            card.RecordDate = DateTime.Now;
            card.RemainingAmount = model.Balance;
            _db.Card.Add(card);
            var save = _db.SaveChanges();
            if (save > 0)
            {
                returnModel.Detail = "Kart başarıyla kaydedildi";
                return returnModel;
            }
            else
            {
                returnModel.IsError = true;
                returnModel.Detail = "Kart kaydedillirken hata oluştu, tekrar deneyiniz!";
                return returnModel;

            }
        }

        public async Task<BaseRequestModel> CardAddAmount(CardAddAmountRequestModel model)
        {
            var returnModel = new BaseRequestModel();
            var checkCard = await _db.Card.Where(t => !t.IsDeleted && t.Id == model.CardId).FirstOrDefaultAsync();
            if (checkCard == null)
            {
                returnModel.IsError = true;
                returnModel.Detail = "Kart bulunamadı";
                return returnModel;
            }
            checkCard.Balance += model.CardAddAmount;
            checkCard.RemainingAmount += model.CardAddAmount;
            var save = _db.SaveChanges();
            if (save > 0)
            {
                returnModel.Detail = "Bakiye yükeme işlemi başarılı";
            }
            else
            {
                returnModel.Detail = "Bakiye yükeme işlemi başarısız";
                returnModel.IsError = true;
            }
            return returnModel;
        }

        public async Task<BaseRequestModel> DeleteCard(ByIdRequestModel model)
        {
            var returnModel = new BaseRequestModel();
            var checkCard = await _db.Card.Where(t => !t.IsDeleted && t.Id == model.Id).FirstOrDefaultAsync();
            if (checkCard == null)
            {
                returnModel.IsError = true;
                returnModel.Detail = "Kart bulunamadı";
            }
            else
            {
                checkCard.IsDeleted = true;
                var save = _db.SaveChanges();
                if (save > 0)
                {
                    returnModel.Detail = "Kart başarıyla silindi";

                }
                else
                {
                    returnModel.IsError = true;
                    returnModel.Detail = "Kart silinemedi, tekrar deneyiniz!";
                }
            }
            return returnModel;
        }


        public async Task<List<GetCardRequestModel>> UpdateCardName(int Id,string FullName)
        {
            var returnModel = new BaseRequestModel();

            var checkCard = await _db.Card.Where(t => !t.IsDeleted && t.Id == Id).FirstOrDefaultAsync();
            if (checkCard != null)
            {
                checkCard.FullName = FullName;

                var save = _db.SaveChanges();
                var cardList = await _db.Card.Where(x => !x.IsDeleted && x.CompanyId == checkCard.CompanyId).ToListAsync();

                var data = cardList.Select(b => b.Adapt<GetCardRequestModel>()).ToList();

                return data;
            }
            else
            {
                return null;
            }


        }
    }
}
