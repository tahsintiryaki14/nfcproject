﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Device
{
    public class AddDeviceRequestModel : ByIdRequestModel
    {
        public string Name { get; set; }
        public string SerialNumber { get; set; }
    }
}
