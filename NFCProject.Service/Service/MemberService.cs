﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Card;
using NFCProject.Contact.RequestModel.Payment;
using NFCProject.Contact.ResponseModel;
using NFCProject.Core.Constants;
using NFCProject.Core.Entities;
using NFCProject.Data.DbContexts;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.Service
{
    public class MemberService : BaseService<Card>, IMemberService
    {
        public MemberService(NFCDbContext db) : base(db)
        {

        }

        public async Task<BaseResultModel> CardPayment(PaymentCardRequestModel model)
        {
            var result = new BaseResultModel();
            if (model != null && !string.IsNullOrEmpty(model.CardGuidId))
            {
                
                if (string.IsNullOrEmpty(model.Amount))
                {
                    result.Detail = "Tutar boş gönderilemez.";
                    result.IsSuccess = 0;

                    return result;
                }

                var amount = decimal.Parse(model.Amount);

                var cardBalance = await _db.Card.Where(b => !b.IsDeleted && b.CardGuidId == model.CardGuidId).FirstOrDefaultAsync();
                if (cardBalance != null)
                {
                    if (decimal.Parse(model.Amount) <= cardBalance.RemainingAmount)
                    {
                        var memberUser = await _db.MemberBusiness.Where(b => !b.IsDeleted && b.UserId == 2).FirstOrDefaultAsync();
                        PaymentDetail paymentDetail = new PaymentDetail();
                        paymentDetail.MemberBusinessId = memberUser.Id;
                        paymentDetail.Amount = amount;
                        paymentDetail.CardId = cardBalance.Id;
                        paymentDetail.RecordDate = DateTime.Now;
                        paymentDetail.ExecutionDate = DateTime.Now;
                        paymentDetail.RecordUserId = 2;

                        await _db.PaymentDetail.AddAsync(paymentDetail);

                        cardBalance.RemainingAmount = cardBalance.RemainingAmount - amount;
                        cardBalance.Balance = cardBalance.Balance - amount;
                        cardBalance.UpdateDate = DateTime.Now;

                        var res = await _db.SaveChangesAsync();
                        if (res > 0)
                        {
                            var paymentDetails = await _db.PaymentDetail.Where(b => !b.IsDeleted && b.MemberBusinessId == model.Id).ToListAsync();
                            //result.Data = paymentDetails.Select(b => b.Adapt<GetPaymentDetailsRequestModel>()).ToList();
                            result.Detail = "Ödeme Başarılı";
                            result.IsSuccess = 1;
                        }
                        else
                        {
                            result.Detail = "Ödeme Başarısız";
                            result.IsError = true;
                            result.IsSuccess = 0;
                        }
                    }
                    else
                    {
                        result.Detail = "Bakiye Yetersiz";
                        result.IsError = true;
                        result.IsSuccess = 0;
                    }
                }
                else
                {
                    result.Detail = "Tanımsız Kart";
                    result.IsSuccess = 0;
                }

            }
            else
            {
                result.Detail = "Ödeme bilgileri alınamadı,tekrar deneyiniz";
                result.IsSuccess = 0;
            }
            return result;
        }

        public async Task<BaseRequestModel> GetAllCards()
        {
            var result = new BaseRequestModel();
            var cards = await _db.Card.Include(b => b.Company).Where(b => !b.IsDeleted).ToListAsync();
            if (cards != null && cards.Count > 0)
            {
                result.Data = cards.Select(b => b.Adapt<GetCardRequestModel>()).ToList();
            }
            else
            {
                result.Data = null;
            }
            result.Detail = "Kartlar Listelendi.";
            return result;
        }
        public async Task<BaseRequestModel> GetPaymentDetailsforMemberBussiness(ByIdRequestModel model)
        {
            var result = new BaseRequestModel();
            var findMember = await _db.MemberBusiness.Where(x => x.UserId == model.Id).FirstOrDefaultAsync();
            if (findMember != null)
            {
                var paymentDetails = await _db.PaymentDetail.Include(b => b.Card).Include(b => b.MemberBusiness).Where(b => !b.IsDeleted && b.MemberBusinessId == findMember.Id).ToListAsync();
                if (paymentDetails != null && paymentDetails.Count > 0)
                {
                    result.Data = paymentDetails.Select(b => b.Adapt<GetPaymentDetailsRequestModel>()).ToList();
                }
                else
                {
                    result.Data = null;
                }
                result.Detail = "Ödeme Hareketleri Listelendi.";
            }
            else
            {
                result.Data = null;
            }
            return result;

        }

        public async Task<BaseRequestModel> GetAllCardsById(ByIdRequestModel model)
        {
            var result = new BaseRequestModel();
            if (model != null)
            {
                var findCompany = await _db.Company.Where(x => x.UserId == model.Id).FirstOrDefaultAsync();

                var cards = await _db.Card.Include(b => b.Company).Where(b => !b.IsDeleted && b.CompanyId == findCompany.Id).ToListAsync();
                if (cards != null && cards.Count > 0)
                {
                    result.Data = cards.Select(b => b.Adapt<GetCardRequestModel>()).ToList();
                }
                else
                {
                    result.Data = null;
                }
                result.Detail = "Kartlar Listelendi.";
                return result;
            }
            else
            {
                result.Data = null;
                result.Detail = "Kart Bulunamadı.";
                return result;
            }

        }

        public async Task<BaseRequestModel> GetFilteredPaymentTransactions(GetFilteredPaymentTransactionsRequestModel model)
        {
            var result = new BaseRequestModel();
            if (model != null)
            {
                var findMember = await _db.MemberBusiness.Where(x => x.UserId == model.Id).FirstOrDefaultAsync();
                if (findMember != null)
                {
                    var paymentDetails = await _db.PaymentDetail.Include(b => b.Card).Include(b => b.MemberBusiness).Where(b => !b.IsDeleted && b.MemberBusinessId == findMember.Id).ToListAsync();
                    if (paymentDetails != null && paymentDetails.Count > 0)
                    {
                        var map = paymentDetails.Select(b => b.Adapt<GetPaymentDetailsRequestModel>()).ToList();

                        if (model.StartDate != null)
                        {
                            map = map.Where(i => i.RecordDate.Date >= model.StartDate.Value.Date).ToList();
                        }

                        if (model.EndDate != null)
                        {
                            map = map.Where(i => i.RecordDate.Date <= model.EndDate.Value.Date).ToList();
                        }

                        result.Data = map;
                        result.Detail = "Ödeme Hareketleri Listelendi";

                    }
                }
                else
                {
                    result.Data = null;
                    result.Detail = "Kullanıcı bulunmadı";
                }
                return result;
            }
            else
            {
                result.Data = null;
                result.Detail = "Filtre bilgileri bulunamadı";
                result.IsError = true;
                return result;
            }
        }

        public async Task<List<ServiceEndustry>> GetAllServiceIndustry()
        {
            var services = await _db.ServiceEndustry.Where(b => !b.IsDeleted).ToListAsync();
            return services;
        }

        public async Task<List<MemberBusiness>> GetAllMemberBusiness()
        {
            var list = await _db.MemberBusiness.Where(b => !b.IsDeleted).Include(x => x.Device).Include(x => x.ServiceEndustry).Include(x => x.User).ToListAsync();
            return list;
        }

        public async Task<BaseRequestModel> AddMemberBusiness(MemberBusiness model)
        {
            var returnModel = new BaseRequestModel();
            Random random = new Random();

            var role = await _db.Role.Where(x => !x.IsDeleted && x.Name == Constants.Role.MemberBusiness.ToString()).FirstOrDefaultAsync();

            User user = new User()
            {
                UserName = random.Next(100000, 999999).ToString(),
                FullName = model.Name,
                Password = "12345",
                RoleId = role.Id,
                RecordDate = DateTime.Now,
            };
            _db.User.Add(user);
            var save = _db.SaveChanges();
            if (save > 0)
            {

                var findDevice = await _db.Device.Where(x => x.Id == model.DeviceId && !x.IsDeleted).FirstOrDefaultAsync();

                MemberBusiness company = new MemberBusiness();
                company.DeviceId = model.DeviceId;
                company.ServiceEndustryId = model.ServiceEndustryId;
                company.UserId = user.Id;
                company.Name = model.Name;
                company.TaxNo = model.TaxNo;
                _db.MemberBusiness.Add(company);

                findDevice.IsEmpty = false;

                _db.SaveChanges();
                returnModel.Detail = "Üye iş yeri başarıyla eklendi";

            }
            else
            {
                returnModel.IsError = true;
                returnModel.Detail = "Üye iş yeri eklenirken hata oluştu, tekrar deneyiniz!";
            }

            return returnModel;
        }

        public async Task<BaseRequestModel> GetPaymentRequest(GetPaymentRequestModel model)
        {
            var result = new BaseRequestModel();

            if (model is null)
            {
                result.Detail = "Lütfen bilgieri boş göndermeyiniz.";
                result.IsSuccess = 0;
                return result;
            }

            if(string.IsNullOrEmpty(model.Amount))
            {
                result.Detail = "Lütfen tutarı boş göndermeyiniz.";
                result.IsSuccess = 0;
                return result;
            }

            var member = _db.MemberBusiness.Where(i => i.Id == model.MemberId && !i.IsDeleted).FirstOrDefault();
            if (member != null)
            {
                var paymentTransaction = new PaymentTransactions();
                paymentTransaction.MemberId = member.Id;
                paymentTransaction.Amount = decimal.Parse(model.Amount);
                paymentTransaction.TransactionKey = Guid.NewGuid().ToString();
                paymentTransaction.RecordDate = DateTime.Now;
                await _db.PaymentTransactions.AddAsync(paymentTransaction);
                var save = await _db.SaveChangesAsync();
                if (save > 0)
                {
                    result.IsSuccess = 1;
                    result.Detail = "Transaction Key Üretildi";
                    result.Data = paymentTransaction.TransactionKey;
                }
                else
                {
                    result.Detail = "Transaction Key Üretilemedi";
                    result.IsSuccess = 0;
                }
            }
            else
            {
                result.Detail = "Firma bulunamadı";
                result.IsSuccess = 0;
            }


            return result;
        }

        public async Task<BaseRequestModel> CompletePaymentRequest(CompletePaymentRequestModel model)
        {
            var result = new BaseRequestModel();

            if (model is null)
            {
                result.Detail = "Lütfen bilgieri boş göndermeyiniz.";
                result.IsSuccess = 0;
                return result;
            }

           
            var paymentTransaction = _db.PaymentTransactions.Where(i => i.TransactionKey == model.TransactionKey && !i.IsUsed).FirstOrDefault();
            if (paymentTransaction != null)
            {
                var card = _db.Card.Where(i => i.CardGuidId == model.CardGuidId && !i.IsDeleted).FirstOrDefault();
                if (card != null)
                {
                    var balance = card.Balance;
                    if (balance >= paymentTransaction.Amount)
                    {
                        paymentTransaction.CardGuidId = model.CardGuidId;
                        paymentTransaction.PaymentDate = DateTime.Now;
                        paymentTransaction.IsUsed = true;

                        var memberUser = await _db.MemberBusiness.Where(b => !b.IsDeleted && b.Id == paymentTransaction.MemberId).FirstOrDefaultAsync();
                        if (memberUser != null)
                        {
                            PaymentDetail paymentDetail = new PaymentDetail();
                            paymentDetail.MemberBusinessId = paymentTransaction.MemberId;
                            paymentDetail.Amount = paymentTransaction.Amount;
                            paymentDetail.CardId = card.Id;
                            paymentDetail.RecordDate = DateTime.Now;
                            paymentDetail.ExecutionDate = DateTime.Now;
                            paymentDetail.RecordUserId = memberUser.UserId;

                            await _db.PaymentDetail.AddAsync(paymentDetail);
                        }
                        else
                        {
                            result.Detail = "Firma bulunamadı";
                            result.IsSuccess = 0;
                            return result;
                        }


                        card.RemainingAmount = card.RemainingAmount - paymentTransaction.Amount;
                        card.Balance = card.Balance - paymentTransaction.Amount;
                        card.UpdateDate = DateTime.Now;

                        var update = await _db.SaveChangesAsync();
                        if (update > 0)
                        {
                            result.IsSuccess = 1;
                            result.Detail = "Ödeme gerçekleşti.";
                        }
                        else
                        {
                            result.Detail = "Ödeme gerçekleşmedi, tekrar deneyiniz.";
                            result.IsSuccess = 0;
                        }
                    }
                    else
                    {
                        result.Detail = "Bakiye Yetersiz";
                        result.IsSuccess = 0;
                    }
                }
                else
                {
                    result.Detail = "Tanımsız Kart";
                    result.IsSuccess = 0;
                }
            }
            else
            {
                result.Detail = "Tanımsız QrCode";
                result.IsSuccess = 0;
            }

            return result;
        }
    }
}
