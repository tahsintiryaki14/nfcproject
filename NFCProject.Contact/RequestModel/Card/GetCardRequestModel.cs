﻿using NFCProject.Contact.RequestModel.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Card
{
    public class GetCardRequestModel
    {
        public int Id { get; set; }
        public string CardGuidId { get; set; }
        public bool IsActive { get; set; }
        public int CompanyId { get; set; }
        public GetCompanyRequestModel Company { get; set; } = new GetCompanyRequestModel();
        public string FullName { get; set; }
        public decimal Balance { get; set; }
        public decimal RemainingAmount { get; set; }
    }
}
