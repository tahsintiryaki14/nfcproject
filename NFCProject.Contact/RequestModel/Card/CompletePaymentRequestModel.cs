﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Card
{
    public class CompletePaymentRequestModel
    {
        public string TransactionKey { get; set; }
        public string CardGuidId { get; set; }  
    }
}
