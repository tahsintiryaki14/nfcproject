﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NFCProject.Data.Migrations
{
    public partial class Updated_PaymentTransactionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "PaymentTransactions",
                newName: "MemberId");

            migrationBuilder.AddColumn<DateTime>(
                name: "PaymentDate",
                table: "PaymentTransactions",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RecordDate",
                table: "PaymentTransactions",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentDate",
                table: "PaymentTransactions");

            migrationBuilder.DropColumn(
                name: "RecordDate",
                table: "PaymentTransactions");

            migrationBuilder.RenameColumn(
                name: "MemberId",
                table: "PaymentTransactions",
                newName: "CompanyId");
        }
    }
}
