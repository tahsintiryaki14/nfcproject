﻿using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Device;
using NFCProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.IService
{
    public interface IDeviceService : IService<Device>
    {
        Task<BaseRequestModel> GetAllDevices();
        Task<BaseRequestModel> AddDevice(AddDeviceRequestModel model);
        Task<BaseRequestModel> DeleteDevice(ByIdRequestModel model);
        Task<BaseRequestModel> UpdateDeviceById(ByIdRequestModel model);
        Task<BaseRequestModel> GetAllEmptyDevices();
    }
}
