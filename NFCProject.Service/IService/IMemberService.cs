﻿using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Card;
using NFCProject.Contact.RequestModel.Payment;
using NFCProject.Contact.ResponseModel;
using NFCProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.IService
{
    public interface IMemberService : IService<Card>
    {
        Task<BaseRequestModel> GetAllCardsById(ByIdRequestModel model);
        Task<BaseResultModel> CardPayment(PaymentCardRequestModel model);
        Task<BaseRequestModel> GetAllCards();
        Task<BaseRequestModel> GetPaymentDetailsforMemberBussiness(ByIdRequestModel model);
        Task<BaseRequestModel> GetFilteredPaymentTransactions(GetFilteredPaymentTransactionsRequestModel model);
        Task<List<ServiceEndustry>> GetAllServiceIndustry();
        Task<List<MemberBusiness>> GetAllMemberBusiness();
        Task<BaseRequestModel> AddMemberBusiness(MemberBusiness model);
        Task<BaseRequestModel> GetPaymentRequest(GetPaymentRequestModel model);
        Task<BaseRequestModel> CompletePaymentRequest(CompletePaymentRequestModel model);
    }
}
