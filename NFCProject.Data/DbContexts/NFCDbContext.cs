﻿using Microsoft.EntityFrameworkCore;
using NFCProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Data.DbContexts
{
    public class NFCDbContext : DbContext
    {
        public NFCDbContext(DbContextOptions<NFCDbContext> options) : base(options)
        {

        }
        public DbSet<User> User { get; set; }
        public DbSet<MemberBusiness> MemberBusiness { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Card> Card { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Device> Device { get; set; }
        public DbSet<PaymentDetail> PaymentDetail { get; set; }
        public DbSet<ServiceEndustry> ServiceEndustry { get; set; }
        public DbSet<PaymentTransactions> PaymentTransactions { get; set; }




        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }


    }
}

