﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.MemberBussiness
{
    public class GetMemberBussinessRequestModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TaxNo { get; set; }
        public int DeviceId { get; set; }
    }
}
