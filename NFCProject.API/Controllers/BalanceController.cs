﻿using Microsoft.AspNetCore.Mvc;
using NFCProject.Contact.RequestModel.Card;
using NFCProject.Service.IService;
using System.Threading.Tasks;

namespace NFCProject.API.Controllers
{
    [Route("balance")]
    [ApiController]
    public class BalanceController : ControllerBase
    {
        private readonly IMemberService _memberService;

        public BalanceController(IMemberService memberService)
        {
            _memberService = memberService;
        }

        [HttpPost]
        [Route("payment")]
        public async Task<IActionResult> Payment(PaymentCardRequestModel model)
        {
            var result = await _memberService.CardPayment(model);

            return Ok(result);
        }


        [HttpPost]
        [Route("getPayment")]
        public async Task<IActionResult> GetPaymentRequest(GetPaymentRequestModel model)
        {
            var result = await _memberService.GetPaymentRequest(model);

            return Ok(result);
        }

        [HttpPost]
        [Route("completePayment")]
        public async Task<IActionResult> CompletePaymentRequest(CompletePaymentRequestModel model)
        {
            var result = await _memberService.CompletePaymentRequest(model);

            return Ok(result);
        }

    }
}
