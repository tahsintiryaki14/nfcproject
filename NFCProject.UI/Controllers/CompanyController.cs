﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Card;
using NFCProject.Contact.RequestModel.Company;
using NFCProject.Core.Entities;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFCProject.UI.Controllers
{
    public class CompanyController : Controller
    {
        private readonly IMemberService _memberService;
        private readonly ICompanyService _companyService;
        private readonly ICardService _cardService;

        public CompanyController(IMemberService memberService, ICompanyService companyService, ICardService cardService)
        {
            _memberService = memberService;
            _companyService = companyService;
            _cardService = cardService;
        }
        public async Task<IActionResult> CardList()
        {
            var model = new ByIdRequestModel();
            var user = HttpContext.Session.GetString("UserId");
            var list = new List<GetCardRequestModel>();
            if (user != null)
            {
                model.Id = Convert.ToInt32(user);
                var data = await _memberService.GetAllCardsById(model);
                list = (List<GetCardRequestModel>)data.Data;
            }
            return View(list);
        }

        public async Task<IActionResult> PaymentDetailList()
        {
            var model = new ByIdRequestModel();
            var user = HttpContext.Session.GetString("UserId");
            var list = new List<PaymentDetail>();
            if (user != null)
            {
                model.Id = Convert.ToInt32(user);
                list = await _companyService.GetAllPaymentDetailByCompanyId(model);
            }


            if (user != null)
            {
                model.Id = Convert.ToInt32(user);
                var data = await _memberService.GetAllCardsById(model);
                ViewBag.CardList = (List<GetCardRequestModel>)data.Data;
            }


            return View(list);
        }

        [HttpPost]
        public async Task<IActionResult> GetAllPaymentDetailByCardId(ByIdRequestModel model)
        {
            var list = await _companyService.GetAllPaymentDetailByCardId(model);

            if (list != null && list.Count > 0)
            {
                return PartialView("~/Views/Company/_PartialPaymentDetailList.cshtml", list);
            }

            return Json(new { failed = true, message = "Kart ödeme detayları bulunamadı" });
        }


        [HttpPost]
        public async Task<IActionResult> UpdateCard(int Id, string FullName)
        {
            var cardList = await _cardService.UpdateCardName(Id, FullName);

            if (cardList != null && cardList.Count > 0)
            {
                return PartialView("~/Views/Company/_PartialCardList.cshtml", cardList);
            }

            return Json(new { failed = true, message = "Kart güncellenirken hata oluştu" });
        }



        public async Task<IActionResult> AddCompany()
        {
            return View();

        }


        public async Task<IActionResult> GetById(int? Id)
        {
            var result = await _companyService.GetById(Id);
            if (result.IsError == true)
            {
                ViewBag.Error = result.Detail;
                return View();
            }
            else
            {
                var getCompany = (GetByIdCompanyRequestModel)result.Data;
                return View(getCompany);
            }


        }
        [HttpPost]
        public async Task<IActionResult> DeleteCompany(int? Id)
        {
            var result = await _companyService.DeleteCompany(Id);
            if (result.IsError == true)
            {
                return Json(new { result = "Error", message = result.Detail });
            }
            else
            {
                return Json(new { result = "Success", message = result.Detail });
            }


        }

        [HttpPost]
        public async Task<IActionResult> AddCompany(AddCompanyRequestModel model)
        {
            var result = await _companyService.AddCompany(model);
            if (result.IsError == true)
            {
                return Json(new { result = "Error", message = result.Detail });
            }
            else
            {
                return Json(new { result = "Success", message = result.Detail });
            }

        }
        public async Task<IActionResult> Index(AddCompanyRequestModel model)
        {
            var result = await _companyService.GetAllCompanies();
            if (result.Data != null)
            {
                ViewBag.Companies = (List<Company>)result.Data;
            }
            return View();

        }

        [HttpPost]
        public async Task<IActionResult> UpdateCompany(GetByIdCompanyRequestModel model)
        {
            var result = await _companyService.UpdateCompany(model);
            if (result.IsError == true)
            {
                return Json(new { result = "Error", message = result.Detail });
            }
            else
            {
                return Json(new { result = "Success", message = result.Detail });
            }

        }

    }
}
