﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Card
{
    public class PaymentCardRequestModel : ByIdRequestModel
    {
        public string CardGuidId { get; set; }
        public string Amount { get; set; }
    }
}
