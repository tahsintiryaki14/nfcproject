﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using NFCProject.Contact.RequestModel;
using NFCProject.Contact.RequestModel.Device;
using NFCProject.Core.Entities;
using NFCProject.Data.DbContexts;
using NFCProject.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.Service
{
    public class DeviceService : BaseService<Device>, IDeviceService
    {
        public DeviceService(NFCDbContext db) : base(db)
        {

        }

        public async Task<BaseRequestModel> AddDevice(AddDeviceRequestModel model)
        {
            var result = new BaseRequestModel();
            if (model != null)
            {
                Device device = new Device();
                device.Name = model.Name;
                device.SerialNumber = model.SerialNumber;
                device.RecordDate = DateTime.Now;
                device.RecordUserId = model.Id;
                device.IsEmpty = true;

                await _db.Device.AddAsync(device);
                var res = _db.SaveChanges();
                if (res == 1)
                {
                    result.Data = null;
                    result.Detail = "Cihaz eklendi.";
                }
                else
                {
                    result.Data = null;
                    result.Detail = "Cihaz eklerken bir hata oluştu,tekrar deneyiniz.";
                    result.IsError = true;
                }
            }
            else
            {
                result.Data = null;
                result.Detail = "Cihaz bilgileri alınamadı.";
                result.IsError = true;
            }
            return result;
        }

        public async Task<BaseRequestModel> DeleteDevice(ByIdRequestModel model)
        {
            var result = new BaseRequestModel();
            if (model != null)
            {
                var device = await _db.Device.Where(b => !b.IsDeleted && b.Id == model.Id).FirstOrDefaultAsync();
                if (device != null)
                {
                    device.IsDeleted = true;
                    device.UpdateDate = DateTime.Now;
                    device.UpdateUserId = model.UserId;

                    var res = _db.SaveChanges();
                    if (res == 1)
                    {
                        result.Data = null;
                        result.Detail = "Cihaz silindi.";
                    }
                    else
                    {
                        result.Data = null;
                        result.Detail = "Cihaz silinirken bir hata oluştu,tekrar deneyiniz.";
                        result.IsError = true;
                    }
                }
            }
            else
            {
                result.Data = null;
                result.Detail = "Cihaz bilgileri alınamadı.";
                result.IsError = true;
            }
            return result;
        }

        public async Task<BaseRequestModel> GetAllDevices()
        {
            var result = new BaseRequestModel();
            var devices = await _db.Device.Include(b=>b.MemberBusinessList).Where(b => !b.IsDeleted).ToListAsync();
            if (devices != null && devices.Count > 0)
            {
                result.Data = devices.Select(b => b.Adapt<GetDeviceRequestModel>()).ToList();
            }
            else
            {
                result.Data = null;
            }
            result.Detail = "Cihazlar Listelendi.";
            return result;
        }

        public async Task<BaseRequestModel> UpdateDeviceById(ByIdRequestModel model)
        {
            var result = new BaseRequestModel();
            if (model != null)
            {
                var device = await _db.Device.Where(b => !b.IsDeleted && b.Id == model.Id).FirstOrDefaultAsync();
                if (device != null)
                {
                    result.Data = device.Adapt<GetDeviceRequestModel>();
                    result.Detail = "Cihaz bilgisi getirildi.";
                }
                else
                {
                    result.Data = null;
                    result.Detail = "Cihaz bulunamadı";
                }
            }
            else
            {
                result.Data = null;
                result.IsError = true;
                result.Detail = "Cihaz bilgileri bulunamadı";
            }
            return result;
        }


        public async Task<BaseRequestModel> GetAllEmptyDevices()
        {
            var result = new BaseRequestModel();
            var devices = await _db.Device.Include(b => b.MemberBusinessList).Where(b => !b.IsDeleted && b.IsEmpty).ToListAsync();
            if (devices != null && devices.Count > 0)
            {
                result.Data = devices.Select(b => b.Adapt<GetDeviceRequestModel>()).ToList();
            }
            else
            {
                result.Data = null;
            }
            result.Detail = "Cihazlar Listelendi.";
            return result;
        }
    }
}
