﻿using Microsoft.EntityFrameworkCore;
using NFCProject.Contact.RequestModel;
using NFCProject.Core.Entities;
using NFCProject.Data.DbContexts;
using NFCProject.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Service.IService
{
    public class UserService : BaseService<User>, IUserService
    {
        public UserService(NFCDbContext db) : base(db)
        {

        }
        public User GetUser(UserRequestModel model)
        {

            var user = _db.User.Where(x => x.UserName == model.UserName && x.Password == model.Password && !x.IsDeleted).Include(x => x.Role).FirstOrDefault();

            return user;

        }
    }
}
