﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Entities
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }
        public string TaxNo { get; set; }
        public string Address { get; set; }
        public string AuthorizedPerson { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public virtual List<Card> CardList { get; set; } = new List<Card>();
    }
}
