﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Contact.RequestModel.Card
{
    public class GetPaymentRequestModel
    {
        public int MemberId { get; set; } 
        public string Amount { get; set; } 

    }
}
