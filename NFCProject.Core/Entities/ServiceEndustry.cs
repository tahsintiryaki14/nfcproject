﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFCProject.Core.Entities
{
    public class ServiceEndustry : BaseEnum
    {
        public ServiceEndustry(string name, string description) : base(name)
        {
            Description = description;
        }
        public string Description { get; set; }
        public virtual List<MemberBusiness> MemberBusinessList { get; set; } = new List<MemberBusiness>();
    }
}
